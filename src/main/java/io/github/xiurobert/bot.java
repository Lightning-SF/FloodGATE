package io.github.xiurobert;

/**
 * Created by xiurobert on 1/31/2015.
 */

/**
 * Fun Class of the FloodGATE bot
 * "Bots are VERY Serious about their jobs"
 * Are they? Nope
 * I present... FloodGATE FUN!
 * @author = xiurobert
 *
 */

/**
 * GNU GPL v3
 *
 * FloodGate IRC - Controls flood better than ever (with some fun)
 * Copyright (C) 2015  Robert Xiu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 */

/**
 * FloodGate IRC  Copyright (C) 2015 Robert Xiu
 * This program comes with ABSOLUTELY NO WARRANTY; for details type `^license gpl3 show w' in the IRC client.
 * This is free software, and you are welcome to redistribute it
 * under certain conditions; type `^license gpl3 show c' in the IRC client for details.
 */

import org.jibble.pircbot.*;

import io.github.xiurobert.fun;

import java.util.Date;

public class bot extends PircBot{
    public bot(){
        this.setName("FloodGATE");
    }
    String owner = "xiurobert";
    protected void onMessage(String channel,
                             String sender,
                             String login,
                             String hostname,
                             String message) {
        if (message.equalsIgnoreCase("^help")) {
            sendNotice(sender, "Here is a list of things I can do: ");
            sendNotice(sender, "help, kick, stop, quit, reconnect, fun");
            sendNotice(sender, "For detailed help, type ^help [command]");
        }
        if (message.equalsIgnoreCase("^help help")){
            sendNotice(sender, ":Gets help");
        }
        if (message.equalsIgnoreCase("^help kick")) {
            sendNotice(sender, ":Kicks [user] from channel (Bot Must be OP)");
        }
        if (message.equalsIgnoreCase("^help stop")) {
            sendNotice(sender, ":Disconnects the bot from the IRC server");
        }
        if (message.equalsIgnoreCase("^help quit")){
            sendNotice(sender, ":PARTS the channel");
        }
        if (message.equalsIgnoreCase("^help reconnect")){
            sendNotice(sender, ":java.io.NullPointerException; This is not implemented yet!");
        }
        if (message.equalsIgnoreCase("^help fun")){
            sendNotice(sender, ":Usage: ^fun [fun type/help]");
            sendNotice(sender, ":Gets fun stuff from the fun class!");
        }
        if (message.equalsIgnoreCase("^stop")) {
            quitServer("FloodGATE has stopped");
        }
        if (message.equalsIgnoreCase("^quit")) {
            partChannel(channel, "FloodGATE leaving channel");
        }
        if (message.equalsIgnoreCase("^kick")) {
            sendNotice(sender, ": Usage: ^kick [nick] | Kicks [nick] from channel (FloodGATE must be opped");
        }
        if (message.contains("fuck"/*This is a bad word*/)){
            kick(sender, "Do not swear");
        }
        if (message.startsWith("^ban")) {
            if(sender.equals(owner))
            {
                String userToBan = message.split(" ")[1];
                ban(channel, userToBan);
                sendMessage(channel, "Banned " + userToBan);
            }
            else
            {
                sendMessage(channel, "You aren't the owner!");
                //embarasses users
            }
        }
        if (message.equalsIgnoreCase("^version")){
            sendNotice(sender, "FloodGATE version 1.0.0-SNAPSHOT");
        }
        if (message.equalsIgnoreCase("^fun")){
            sendNotice(sender, "Usage: ^fun [action/help]");
        }
        if (message.equalsIgnoreCase("^fun help")){
            sendNotice(sender, "Here is a list of things I can do");
            sendNotice(sender, "java, time, derp, potato, ping, google, spotify,");
            sendNotice(sender, "jre, kill, mcping, punch, smack, hit, box, mailaway, putinatrashcan");
            sendNotice(sender, "version");
        }
    }
    /*
    public void onMesage(String channel, String sender, String login, String message){
        if (message.equalsIgnoreCase("^fun java")){
            //Nothing implemented yet
        }
        if (message.equalsIgnoreCase("^fun time")){
            String time = new Date().toString();
            sendNotice(sender, "The server time is now"+time+"(Singapore Standard Time)");
        }
    }
    */


    public final void onInvite(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel){
        joinChannel(channel);
    }
}
