package io.github.xiurobert;

/**
 * Created by xiurobert on 1/29/2015.
 */

/**
 * GNU GPL v3
 *
 * FloodGate IRC - Controls flood better than ever (with some fun)
 * Copyright (C) 2015  Robert Xiu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 */

/**
 * FloodGate IRC  Copyright (C) 2015 Robert Xiu
 * This program comes with ABSOLUTELY NO WARRANTY; for details type `^license gpl3 show w' in the IRC client.
 * This is free software, and you are welcome to redistribute it
 * under certain conditions; type `^license gpl3 show c' in the IRC client for details.
 */

public class botMain{
    public static void main(String[] args) throws Exception{
        // Now start our bot up.
        bot bot = new bot();

        // Enable debugging output.
        bot.setVerbose(true);

        // Connect to the IRC server.
        bot.connect("irc.spi.gt");


        //Change Nick
        bot.changeNick("FloodGATE");

        // Join the #bitcraft-mc Channel.
        bot.joinChannel("#bitcraft-mc");

        //Makes bot set USER MODE B
        bot.sendCTCPCommand("FloodGATE", "MODE FloodGATE +B");

        //TEMP: Register bot
        bot.sendAction("FloodGate", "MSG NickServ REGISTER floodgate rxz160@gmail.com");
    }

}
