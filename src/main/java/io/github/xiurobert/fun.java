package io.github.xiurobert;


import java.util.Random;

/**
 * Created by xiurobert on 1/31/2015.
 */

/**
 * GNU GPL v3
 *
 * FloodGate IRC - Controls flood better than ever (with some fun)
 * Copyright (C) 2015  Robert Xiu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 */

/**
 * FloodGate IRC  Copyright (C) 2015 Robert Xiu
 * This program comes with ABSOLUTELY NO WARRANTY; for details type `^license gpl3 show w' in the IRC client.
 * This is free software, and you are welcome to redistribute it
 * under certain conditions; type `^license gpl3 show c' in the IRC client for details.
 */

/**
 * GNU GPL is included in the file COPYING.txt
 */
public class fun extends bot{
    public void onMessage(String channel, String sender, String login, String hostname, String message){
        if (message.equalsIgnoreCase("^fun")){
            sendNotice(sender, "Usage: ^fun [action/help]");
        }

        if (message.equalsIgnoreCase("^fun help")){
            sendNotice(sender, "Here is a list of things I can do");
            sendNotice(sender, "java, " +
                    "time, " +
                    "derp, " +
                    "potato, " +
                    "ping, " +
                    "google, " +
                    "spotify, " +
                    "jre, " +
                    "kill, " +
                    "mcping, " +
                    "punch, " +
                    "smack, " +
                    "hit, " +
                    "box, " +
                    "mailaway, " +
                    "putinatrashcan"+
                    "randomnumber, " +
                    "version, " +
                    "clearchathistory, " +
                    "jump, " +
                    "randomimage");
                    sendNotice(sender, channel + ":FloodGATE Bot by xiurobert");
        }

        if (message.equalsIgnoreCase("^fun randomnumber")){
            Random rand = new Random();
            int n = rand.nextInt();
            sendNotice(sender, "Your lucky random number is..."+n+"!");
        }
    }
}
