# Floodgate IRC Bot

## Building
1. Download ZIP File
2. Extract the contents of the file to somewhere (On windows, It should be in C:\your\favourite\location. Other OSes, it should be in /your/favourite/location)
3. Install Maven (Find out how yourself)
4. Then, type in `cd "C:\your\favourite\location"` if you are on windows, `cd "/your/favourite/location"` on other OSes
5. Run `mvn clean package`
-----------------

### Travis-CI Build status
[![Build Status](https://travis-ci.org/xiurobert/FloodGATE.svg?branch=master)](https://travis-ci.org/xiurobert/FloodGATE)

#### Changes
Converted to Maven

######Dependencies
pIRCbot (Maven fetches it)
#####################
#######WARNING#######
#####################
If the maven repository is down, you MUST get your own pircbot.jar file
